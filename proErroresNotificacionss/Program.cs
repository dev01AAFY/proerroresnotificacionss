﻿using proErroresNotificacionss.Utils;
using System;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace proErroresNotificacionss
{
    class Program
    {
        static void Main(string[] args)
        {
            string sErrorMsg = string.Empty;
            JObject oJReferencias = null;
            string sAmbiente = string.Empty;
            string sdtFechaIni = string.Empty;
            string sdtFechaFin = string.Empty;
            JArray listResultado = new JArray();
            string sQuery = string.Empty;
            SqlCommand oSqlCmd = null;
            string sCSV = "iIdProcesaPago, iIdSistema, iError, cError, cReferenciaBanco_, cReferencia_, cFolioTT, dtFechaAlta, \n";
            string sCadenaConnect = string.Empty;
            string sConexionBD = string.Empty;
            string strFilePath = string.Empty;
            string sNameCsv = string.Empty;


            try
            {
                if (args.Length > 0)
                {
                    Utilerias.guardarLogEventos("INICIO DE SERVICIO");

                    sAmbiente = args[0].Trim().ToUpper();

                    switch (args.Length)
                    {
                        case 1:
                            //sNameCsv = DateTime.Now.ToString("yyyy-MM-dd");
                            sdtFechaIni = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:01";
                            sdtFechaFin = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59";
                            break;
                        case 2:
                            //sNameCsv = args[1].Trim();
                            sdtFechaIni = args[1].Trim();
                            sdtFechaIni += " 00:00:01";
                            sdtFechaFin = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59";
                            break;
                        case 3:
                            //sNameCsv = args[1].Trim() + "-to-" + args[2].Trim();
                            sdtFechaIni = args[1].Trim();
                            sdtFechaIni += " 00:00:01";
                            sdtFechaFin = args[2].Trim();
                            sdtFechaFin += " 23:59:59";
                            break;
                    }//fin:switch (args.Length)

                    //Obtener la cadena encriptada de la conexión a la base de datos
                    switch (sAmbiente)
                    {
                        case Constantes._DESARROLLO:
                            sCadenaConnect = ConfigurationManager.AppSettings[Constantes.DES_CONEXION_DB].Trim();
                            break;
                        case Constantes._PRODUCCION:
                            sCadenaConnect = ConfigurationManager.AppSettings[Constantes.PROD_CONEXION_DB].Trim();
                            break;
                    }//fin:sAmbiente

                    //OBTENER VARIABLES DE CONFIGURACIÓN
                    sQuery = Utilerias.ObtenerQueryXML(Constantes.QUERY_SELECT_REGISTROS, ref sErrorMsg);
                    sConexionBD = StringCipher.Decrypt(sCadenaConnect, Constantes.CIPHER_KEY_ENCRIPTA);
                    sNameCsv = ConfigurationManager.AppSettings[Constantes.NOMBRE_CSV].Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + sAmbiente + ".csv";
                    strFilePath = ConfigurationManager.AppSettings[Constantes.DIRECTORIO_CSV].Trim() + sNameCsv;
                    String sAsunto = "[" + sAmbiente + "] " + ConfigurationManager.AppSettings[Constantes.EMAIL_ASUNTO].Trim();
                    String sPlantillaEmail = ConfigurationManager.AppSettings[Constantes.EMAIL_PLANTILLA].Trim();
                    String sMensaje = sdtFechaIni + " y " + sdtFechaFin;
                    Dictionary<string, string> dictRemplazos = new Dictionary<string, string> { { "#fechas#", sMensaje } };

                    if (string.IsNullOrEmpty(sErrorMsg))
                    {
                        //INICIO PETICIÓN A SQL SERVER
                        using (SqlConnection oConn = new SqlConnection(sConexionBD))
                        {
                            oSqlCmd = new SqlCommand(sQuery, oConn);
                            oSqlCmd.CommandType = CommandType.Text;
                            oSqlCmd.Parameters.Add("@dtFechaIni", SqlDbType.VarChar);
                            oSqlCmd.Parameters["@dtFechaIni"].Value = sdtFechaIni;
                            oSqlCmd.Parameters.Add("@dtFechaFin", SqlDbType.VarChar);
                            oSqlCmd.Parameters["@dtFechaFin"].Value = sdtFechaFin;
                            oConn.Open();
                            using (SqlDataReader odtReader = oSqlCmd.ExecuteReader())
                            {
                                if (odtReader.HasRows)
                                {
                                    while (odtReader.Read())
                                    {
                                        oJReferencias = new JObject();
                                        oJReferencias["iIdProcesaPago"] = odtReader["iIdProcesaPago"].ToString();
                                        oJReferencias["iIdSistema"] = odtReader["iIdSistema"].ToString();
                                        oJReferencias["iError"] = odtReader["iError"].ToString();
                                        oJReferencias["cError"] = odtReader["cError"].ToString().Replace(",", "");
                                        oJReferencias["cReferenciaBanco_"] = odtReader["cReferenciaBanco_"].ToString();
                                        oJReferencias["cReferencia_"] = odtReader["cReferencia_"].ToString();
                                        oJReferencias["cFolioTT"] = odtReader["cFolioTT"].ToString();
                                        oJReferencias["dtFechaAlta"] = Convert.ToDateTime(odtReader["dtFechaAlta"]).ToString("yyyy-MM-dd HH:mm:ss");
                                        listResultado.Add(oJReferencias);

                                        sCSV += oJReferencias["iIdProcesaPago"] + ",";
                                        sCSV += oJReferencias["iIdSistema"] + ",";
                                        sCSV += oJReferencias["iError"] + ",";
                                        sCSV += oJReferencias["cError"] + ",";
                                        sCSV += oJReferencias["cReferenciaBanco_"] + ",";
                                        sCSV += oJReferencias["cReferencia_"] + ",";
                                        sCSV += oJReferencias["cFolioTT"] + ",";
                                        sCSV += oJReferencias["dtFechaAlta"] + ", \n";

                                    }//fin:while (odtReader.Read())
                                }
                                else
                                {
                                    Console.WriteLine("NO EXISTE REGISTROS EN EL RANGO DE FECHA");
                                    Utilerias.guardarLogEventos("ERROR: EL RANGO DE FECHA NO TIENE REGISTOS");
                                }
                            }//fin: using (SqlDataReader reader = oSqlCmd.ExecuteReader())
                            oConn.Close();

                            if (listResultado.Count > 0)
                            {
                                //CREAR CSV Y ENVIAR CORREO
                                File.AppendAllText(strFilePath, sCSV.ToString(), Encoding.UTF8);
                                Utilerias.EnviarEmailCSV(true, sAmbiente, sAsunto, sPlantillaEmail, new List<string> { strFilePath }, dictRemplazos, ref sErrorMsg);

                                if (string.IsNullOrEmpty(sErrorMsg))
                                {
                                    Console.Write("CORREO ENVIADO");
                                }
                                else
                                {
                                    Console.WriteLine(sErrorMsg);
                                    Utilerias.guardarLogEventos(sErrorMsg);
                                }
                            }//FIN: DE IF(listResultado.Count > 0)

                        }
                        //FIN PETICIÓN A SQL SERVER
                    }
                    Utilerias.guardarLogEventos("FIN DE SERVICIO");
                }
                else
                {
                    Console.WriteLine("El número de parámetros son incorrectos.");
                }

            }
            catch (Exception oEx)
            {
                Console.WriteLine(oEx.Message);
                Utilerias.guardarLogEventos(oEx.Message);
            }
            Console.Write(listResultado.ToString());
            Console.ReadKey();
        }
    }
}
