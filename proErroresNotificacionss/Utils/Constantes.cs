﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proErroresNotificacionss.Utils
{
    class Constantes
    {
        public const int TIMEOUT_DEFAULT = 600000;
        public const int _ESTATUS_PAGADO = 2;
  
        public const string LOG_FILENAME = "proErroresNotificacionss.log";

        public const string DIRECTORIO_LOG = "DIRECTORIO_LOG";
        public const string FILE_XML_QUERY = "FILE_XML_QUERY";
        public const string DIRECTORIO_OUTPUT = "DIRECTORIO_OUTPUT";
        public const string DIRECTORIO_CSV = "DIRECTORIO_CSV";

        public const string CIPHER_KEY_ENCRIPTA = "K3Y_M0T0R_G3N3R1C0_W3B";

        public const string _DESARROLLO = "DESARROLLO";
        public const string _PRODUCCION = "PRODUCCION";

        public const string DES_CONEXION_DB = "DES_CONEXION_DB";
        public const string PROD_CONEXION_DB = "PROD_CONEXION_DB";

        public const string EMAIL_USUARIO = "EMAIL_USUARIO";
        public const string EMAIL_ASUNTO = "EMAIL_ASUNTO";
        public const string EMAIL_AVISO = "EMAIL_AVISO";
        public const string EMAIL_MENSAJE = "EMAIL_MENSAJE";
        public const string EMAIL_PASSWORD = "EMAIL_PASSWORD";
        public const string EMAIL_SMTP_HOST = "EMAIL_SMTP_HOST";
        public const string EMAIL_FROM = "EMAIL_FROM";
        public const string EMAIL_PORT = "EMAIL_PORT";
        public const string EMAIL_CC = "EMAIL_CC";
        public const string EMAIL_CCO = "EMAIL_CCO";
        public const string NOMBRE_CSV = "NOMBRE_CSV";
        public const string EMAIL_ERROR_TO = "EMAIL_ERROR_TO";
        public const string DES_EMAIL_PARA = "DES_EMAIL_PARA";
        public const string PROD_EMAIL_PARA = "PROD_EMAIL_PARA";
        public const string EMAIL_PLANTILLA = "EMAIL_PLANTILLA";

        public const string _ROOT_XML_QUERY = "querys";

        public const string QUERY_SELECT_REGISTROS = "select-registros";
    }
}
