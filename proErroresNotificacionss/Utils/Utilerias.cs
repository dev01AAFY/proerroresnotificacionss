﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace proErroresNotificacionss.Utils
{
    class Utilerias
    {
        //---------------------------------------------------------------------
        private static string CrearBodyEmail(string sAviso, string sMensaje)
        {

            string stBody = string.Empty;

            stBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                   " <html xmlns='http://www.w3.org/1999/xhtml'> " +
                   "<head> " +
                   "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />" +
                   "<title>Avisos</title>" +
                   "</head>" +
                   "<body>" +
                   "<table cellpadding='0' cellspacing='0' border='0' width='500'>" +
                   "<tr>" +
                   "   <td height='30' width='500'><font face='Arial, Helvetica, sans-serif' size='2'><b>Aviso</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'>" + sAviso + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'><b>Descripci&oacute;n</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2' color='#FF0000'>" + sMensaje + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='25'></td>" +
                   "</tr>" +
                   "</table>" +
                   "</body>" +
                   "</html>";
            return stBody;
        }//fin:CrearBodyEmail
        //---------------------------------------------------------------------
        public static string ObtenerQueryXML(string _sNodoXml, ref string _sOutError)
        {
            string stQuery = string.Empty;
            string sFileXml = ConfigurationManager.AppSettings[Constantes.FILE_XML_QUERY].Trim();
            
            XmlDocument oDocXml = null;
            XmlNode oNodeXml = null;

            try
            {
                oDocXml = new XmlDocument();
                oDocXml.Load(sFileXml);
                _sOutError = string.Empty;

                oNodeXml = oDocXml.DocumentElement.SelectSingleNode("//" + Constantes._ROOT_XML_QUERY + "/" + _sNodoXml);
                if (oNodeXml != null)
                {
                    stQuery = oNodeXml.InnerText.Trim();
                }//fin:if
                else
                {
                    _sOutError = "Error - No existe el nodo :" + _sNodoXml;
                }//fin:
                oNodeXml = null;
            }
            catch (Exception oEx)
            {
                stQuery = string.Empty;
                _sOutError = "Error en el método [ObtenerQueryXML]: ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
            }
            return stQuery;
        }//fin:ObtenerQueryXML
        //---------------------------------------------------------------------
        public static void EnviarEmail(string _Asunto, string _Aviso, string _Mensaje, string filename)
        {

            bool lOkConfigEmail = false;
            string stBody = string.Empty;
            string stEmailPort = string.Empty;
            string stSMTP_User = string.Empty;
            string stSMTP_Pass = string.Empty;
            string stSMTP_Host = string.Empty;
            string stErrors = string.Empty;
            MailMessage oMailMessage = null;
            Attachment data = new Attachment(filename, MediaTypeNames.Application.Octet);
            Dictionary<string, string> oDictConfig = null;

            try
            {
                ConfigurationManager.RefreshSection("appSettings");
                oDictConfig = new Dictionary<string, string>();

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_USUARIO] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_USUARIO,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_USUARIO].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_PASSWORD] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_PASSWORD,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_PASSWORD].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_SMTP_HOST] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_SMTP_HOST,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_SMTP_HOST].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_ERROR_TO] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_ERROR_TO,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_ERROR_TO].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_FROM] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_FROM,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_FROM].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_PORT] != null)
                {
                    oDictConfig.Add(Constantes.EMAIL_PORT,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_PORT].Trim());
                }//fin:if

                lOkConfigEmail = oDictConfig.ContainsKey(Constantes.EMAIL_USUARIO) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_PASSWORD) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_SMTP_HOST) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_FROM) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_PORT);

                if (lOkConfigEmail)
                {
                    oMailMessage = new MailMessage();

                    //Body
                    stBody = Utilerias.CrearBodyEmail(_Aviso, _Mensaje);
                    oMailMessage.Body = stBody;
                    oMailMessage.IsBodyHtml = true;

                    //Para [,]
                    oMailMessage.To.Add(oDictConfig[Constantes.EMAIL_ERROR_TO].Replace(";", ","));

                    //De
                    oMailMessage.From = new MailAddress(oDictConfig[Constantes.EMAIL_FROM]);

                    //Asunto
                    oMailMessage.Subject = _Asunto;

                    //Archivo
                    oMailMessage.Attachments.Add(data);

                    //Credenciales
                    stSMTP_User = oDictConfig[Constantes.EMAIL_USUARIO];
                    stSMTP_Pass = oDictConfig[Constantes.EMAIL_PASSWORD];

                    //Asignacion del puerto
                    stEmailPort = oDictConfig[Constantes.EMAIL_PORT];

                    //Asignacion del Host
                    stSMTP_Host = oDictConfig[Constantes.EMAIL_SMTP_HOST].Trim();
                    oDictConfig = null;
                    using (SmtpClient oSMTP = new SmtpClient())
                    {
                        oSMTP.Host = stSMTP_Host;
                        if (stEmailPort != null && stEmailPort != "0" && stEmailPort != "")
                        {
                            oSMTP.Port = int.Parse(stEmailPort);
                            oSMTP.EnableSsl = true;
                        }//fin:if
                        oSMTP.Credentials = new NetworkCredential(stSMTP_User, stSMTP_Pass);
                        oSMTP.Send(oMailMessage);

                        oMailMessage.Dispose();
                        oSMTP.Dispose();
                    }//fin: using (System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient())
                }//fin: if (lOkConfigEmail)                
            }//fin:try
            catch (Exception oEx)
            {
                stErrors = "En [EnviarEmail] ocurrió el siguiente error: "; ;
                stErrors += "Mensaje^" + oEx.Message;
                stErrors += "~Source^" + oEx.Source;
                stErrors += "~Target^" + oEx.TargetSite.ToString();
                stErrors += "~StackTrace^" + oEx.StackTrace;
            }
        }//fin:EnviaEmail 
         //---------------------------------------------------------------------
        public static void grabarLog(string _path, string _linea)
        {

            string stFullFileLog = "";
            string stLogMessage = "";

            try
            {
                if (!string.IsNullOrEmpty(_path))
                {
                    Encoding EncodeLog = Encoding.UTF8;
                    stFullFileLog = Path.Combine(_path, (DateTime.Now.ToString("yyyyMMdd") + "_" + Constantes.LOG_FILENAME));
                    stLogMessage = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + _linea + Environment.NewLine;
                    if (!Directory.Exists(_path)) { Directory.CreateDirectory(stFullFileLog); }
                    using (FileStream fs = new FileStream(stFullFileLog, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs, EncodeLog))
                        {
                            sw.Write(stLogMessage);
                            sw.Close();
                        }//Fin:using(StreamWriter sw = new StreamWriter(fs))    
                    }//fin:using
                }//fin:if                
            }//fin:try 
            catch {; }
            //return lSuccess;
        }//fin:sWriteLineDate                               
        //---------------------------------------------------------------------
        public static void guardarLogEventos(string sLineaLog)
        {

            string stRutaLog = string.Empty;

            ConfigurationManager.RefreshSection("appSettings");
            if (ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG] != null)
            {
                stRutaLog = ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG].Trim();
            }//fin:if

            if (!string.IsNullOrEmpty(stRutaLog))
            {
                Utilerias.grabarLog(stRutaLog, sLineaLog);
            }//fin:if                                
        }//fin:GuardarLogEventos
        //---------------------------------------------------------------------
        public static void EnviarEmailCSV(
            bool _bUsaPlantilla,
            string _Ambiente,
            string _Asunto,
            string _sPlantilla,
            List<string> _listAdjuntos,
            Dictionary<string, string> _dictReemplazos,
            ref string _sOutError)
        {

            bool bContinue = true;
            string stBody = string.Empty;
            string stEmailPort = string.Empty;
            string stSMTP_User = string.Empty;
            string stSMTP_Pass = string.Empty;
            string stSMTP_Host = string.Empty;

            string stCorreoFrom = string.Empty;
            string stCorreoTo = string.Empty;
            string stEmailCC = string.Empty;
            string stEmailCCO = string.Empty;

            MailMessage oMailMessage = null;

            try
            {
                _sOutError = "";

                ConfigurationManager.RefreshSection("appSettings");

                stSMTP_User = ConfigurationManager.AppSettings[Constantes.EMAIL_USUARIO].Trim();
                stSMTP_Pass = ConfigurationManager.AppSettings[Constantes.EMAIL_PASSWORD].Trim();
                stSMTP_Host = ConfigurationManager.AppSettings[Constantes.EMAIL_SMTP_HOST].Trim();
                stCorreoFrom = ConfigurationManager.AppSettings[Constantes.EMAIL_FROM].Trim();
                stEmailPort = ConfigurationManager.AppSettings[Constantes.EMAIL_PORT].Trim();
                stEmailCC = ConfigurationManager.AppSettings[Constantes.EMAIL_CC].Trim();
                stEmailCCO = ConfigurationManager.AppSettings[Constantes.EMAIL_CCO].Trim();

                switch (_Ambiente)
                {
                    case Constantes._DESARROLLO:
                        stCorreoTo = ConfigurationManager.AppSettings[Constantes.DES_EMAIL_PARA].Trim();
                        break;

                    case Constantes._PRODUCCION:
                        stCorreoTo = ConfigurationManager.AppSettings[Constantes.PROD_EMAIL_PARA].Trim();
                        break;
                }//fin:iAmbienteValue

                oMailMessage = new MailMessage();

                if (_bUsaPlantilla)
                {
                    if (File.Exists(_sPlantilla))
                    {
                        stBody = File.ReadAllText(_sPlantilla);
                    }//fin:if
                    else
                    {
                        bContinue = false;
                        _sOutError = "El archivo de la plantilla(html) no fue localizada.";
                    }//fin:
                }//fin:
                else
                {
                    _sPlantilla = string.Empty;
                    stBody = CrearBodyEmail(_Asunto, "&nbsp;");
                }//fin:else

                if (bContinue)
                {
                    //Reemplazos en plantilla
                    if (_dictReemplazos != null)
                    {
                        foreach (KeyValuePair<string, string> oKeyPReemplazo in _dictReemplazos)
                        {
                            stBody = stBody.Replace(oKeyPReemplazo.Key, oKeyPReemplazo.Value);
                        }//fin:iforeach                    
                    }//fin:else

                    //Asignar los archivos adjuntos
                    if (_listAdjuntos != null)
                    {
                        foreach (string sFileAdd in _listAdjuntos)
                        {
                            if (File.Exists(sFileAdd))
                            {
                                oMailMessage.Attachments.Add(new Attachment(sFileAdd));
                            }//fin:if                        
                        }//fin:foreach
                    }//fin:if (_listAdjuntos != null)

                    //Body                
                    oMailMessage.Body = stBody;
                    oMailMessage.IsBodyHtml = true;

                    //Para [,]
                    oMailMessage.To.Add(stCorreoTo.Replace(";", ","));

                    //De
                    oMailMessage.From = new MailAddress(stCorreoFrom);

                    //CC
                    if (!string.IsNullOrEmpty(stEmailCC))
                    {
                        oMailMessage.CC.Add(stEmailCC.Replace(";", ","));
                    }//fin:if

                    //CC0
                    if (!string.IsNullOrEmpty(stEmailCCO))
                    {
                        oMailMessage.Bcc.Add(stEmailCCO.Replace(";", ","));
                    }//fin:if

                    //Asunto
                    oMailMessage.Subject = _Asunto;

                    //Asignacion del Host                            
                    using (SmtpClient oSMTP = new SmtpClient())
                    {
                        oSMTP.Host = stSMTP_Host;
                        if (stEmailPort != null && stEmailPort != "0" && stEmailPort != "")
                        {
                            oSMTP.Port = int.Parse(stEmailPort);
                        }//fin:if     

                        oSMTP.EnableSsl = true;
                        oSMTP.Timeout = 20000;
                        oSMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                        oSMTP.UseDefaultCredentials = false;
                        oSMTP.Credentials = new NetworkCredential(stSMTP_User, stSMTP_Pass);
                        oSMTP.Send(oMailMessage);

                        oMailMessage.Dispose();
                        oSMTP.Dispose();
                    }//fin: using (System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient())
                }//fin:if                
            }//fin:try
            catch (Exception oEx)
            {
                _sOutError = "Error en [EnviarEmail]: ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
            }
        }//fin:EnviarEmail
        //-----------------------------------------------------------------------------------------
    }
}
